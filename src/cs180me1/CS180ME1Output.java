package cs180me1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class CS180ME1Output {
    protected PrintWriter output;
    
    public CS180ME1Output(String filepath) {
        File outFile = new File(filepath);
        try {
            output = new PrintWriter(new FileWriter(outFile, false));
        }
        catch (IOException e) {
            System.err.println("Invalid output file.");
        }
        catch (Exception e) {
            System.err.println("Unknown error occurred.");
            e.printStackTrace();
        }   
    }
    
    public void print(String s) { output.print(s); }
    public void println(String s) { output.println(s); }
    
    public void printArrayList(ArrayList al) {
        for (Object e : al) {
            output.println(e.toString());
        }
    }
    
    public void printArrayList(ArrayList al, String delimiter) {
        for (Object e : al) {
            output.print(e.toString());
            output.print(delimiter);
        }
    }
    
    public void close() {
        output.close();
    }
}
