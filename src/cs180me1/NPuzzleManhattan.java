package cs180me1;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class NPuzzleManhattan implements Heuristic {
    public int size;
    
    public NPuzzleManhattan(int size) {
        this.size = size;
    }
    
    @Override
    public int getDistance(String value, String goal) {
        int distance = 0;
        for (int i = 0; i < goal.length(); i++) {
            char letter = goal.charAt(i);
            if (letter != '0') {
                int pos = value.indexOf(letter);

                int goal_x = i % size;
                int goal_y = i / size;
                int state_x = pos % size;
                int state_y = pos / size;

                distance += Math.abs(goal_x - state_x) + Math.abs(goal_y - state_y);
            }
        }
        return distance;
    }

}
