package cs180me1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Stack;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class NPuzzleState extends AStarState {
    // specific fields
    public int size;
    
    public ArrayList<String> move_path;
    
    // root constructor
    public NPuzzleState(int size, String start, String goal, Heuristic h) {
        super(start, goal, h);
        this.size = size;
        move_path = new ArrayList();
    }
    
    // child constructor
    protected NPuzzleState (AStarState parent, String value, String move) {
        super(parent, value);
        this.size = ((NPuzzleState)parent).size;
        move_path = new ArrayList(((NPuzzleState)parent).move_path);
        move_path.add(move);
    }
    
    // move
    String move (int tiles_to_move) {
        int blank_tile = value.indexOf("0");
        int new_tile = blank_tile + tiles_to_move;
        try {
            if (new_tile < 0) {
                throw new Exception("Can no longer move up or left.");
            }
            if (new_tile >= value.length()) {
                throw new Exception("Can no longer move down or right.");
            }
            if (new_tile > blank_tile && new_tile % size < blank_tile % size) {
                throw new Exception("Can no longer move right.");
            }
            if (new_tile < blank_tile && new_tile % size > blank_tile % size) {
                throw new Exception("Can no longer move left.");
            }
        }
        catch (Exception e) {
            return null;
        }
        StringBuilder val = new StringBuilder(value);
        char tmp = val.charAt(new_tile);
        val.setCharAt(new_tile, '0');
        val.setCharAt(blank_tile, tmp);
        return val.toString();
    }
    
    String moveUp() { return move(-size); }
    String moveDown() { return move(size); }
    String moveLeft() { return move(-1); }
    String moveRight() { return move(1); }
    
    // inversions
    int getInversions() {
        int inversions = 0;
        
        StringBuilder start_t = new StringBuilder(start);
        StringBuilder goal_t = new StringBuilder(goal);
        start_t.deleteCharAt(start_t.indexOf("0"));
        goal_t.deleteCharAt(goal_t.indexOf("0"));

        for (int i = 0; i < goal_t.length() - 1; i++) {
            for (int j = i; j < goal_t.length(); j++) {
                char letter1 = goal_t.charAt(i);
                char letter2 = goal_t.charAt(j);
                if (start_t.indexOf(Character.toString(letter1)) > start_t.indexOf(Character.toString(letter2))) {
                    inversions += 1;
                }
            }
        }
        
        return inversions;
    }
    
    // override
    @Override
    boolean isSolvable() {
        if (size % 2 == 0) {
            int blank_tile = value.indexOf("0");
            int row = blank_tile / size;
            return (getInversions() + row) % 2 > 0;
        }
        else {
            return getInversions() % 2 == 0;
        }
    }
    
    @Override
    AStarState generateUnsolvableChild(String value) {
        NPuzzleState child = new NPuzzleState(this, value, "UNSOLVABLE");
        if (children == null) {
            children = new HashSet();
        }
        children.add(child);
        return child;
    }
    
    @Override
    void generateChildren(HashSet<String> visited) {
        if (children == null) {
            children = new HashSet();
            
            String child_val;
            String move;
            for (int i = 0; i < 4; i++) {
                switch (i) {
                    case 0:
                        child_val = this.moveUp();
                        move = "UP";
                        break;
                    case 1:
                        child_val = this.moveDown();
                        move = "DOWN";
                        break;
                    case 2:
                        child_val = this.moveLeft();
                        move = "LEFT";
                        break;
                    case 3:
                        child_val = this.moveRight();
                        move = "RIGHT";
                        break;
                    default:
                        child_val = null;
                        move = null;
                }
                if (child_val == null) { continue; }
                if (visited.contains(child_val)) { continue; }
                try {
                    if (parent.value.equals(child_val)) { continue; }
                }
                catch (NullPointerException e) {
                }
                NPuzzleState child = new NPuzzleState(this, child_val, move);
                children.add(child);
            }
        }
    }
    
    // representation functions
    public String addWhitespaces(String val) {
        String str = "";
        for (int i = 0; i < val.length(); i++) {
            str += val.charAt(i);
            str += (i % size == size-1) ? System.lineSeparator() : " ";
        }
        return str;
    }
    
    @Override
    public String toString() {
        return addWhitespaces(value);
    }
    
    public ArrayList<String> getConfigPath() {
      Stack<String> config_stack = new Stack();
      AStarState node = this;
      while (node.parent != null) {
        if (node.value.equals("UNSOLVABLE"))
          config_stack.push(node.value);
        else
          config_stack.push(addWhitespaces(node.value));
        node = node.parent;
      }
      
      ArrayList<String> config_path = new ArrayList();
      while (!config_stack.isEmpty())
        config_path.add(config_stack.pop());
      
      return config_path;
    }
    
}
