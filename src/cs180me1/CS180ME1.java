/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */

package cs180me1;

import java.util.ArrayList;
import java.util.Hashtable;

public class CS180ME1 {
    public static final int NPUZZLE_SIZE = 3;
    public static String in_s = "start";
    public static String in_g = "goal";
    public static String out_c = "configs";
    public static String out_m = "moves";
    public static String heuristic = "composite";
    public static Hashtable<String, Heuristic> heuristics;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        processArgs(args);
        
        initializeHeuristics();
        
        NPuzzleState startState = initializeNPuzzle();
        
        AStarSolver solver = new AStarSolver(startState);
        solver.solve();
        
        NPuzzleState goalState = (NPuzzleState)solver.goalState;
        ArrayList<String> config_list = goalState.getConfigPath();
        ArrayList<String> move_list = goalState.move_path;
        
        String run_data = System.lineSeparator();
        run_data += "Moves: " + move_list.size() + System.lineSeparator();
        run_data += "Heuristic: " + heuristic + System.lineSeparator();
        run_data += "Execution time: " + solver.getTimeElapsed() + "ms";
        
        CS180ME1Output configs = new CS180ME1Output(out_c);
        configs.printArrayList(config_list);
        configs.print(run_data);
        configs.close();
        
        CS180ME1Output moves = new CS180ME1Output(out_m);
        moves.printArrayList(move_list);
        moves.print(run_data);
        moves.close();
    }
    
    public static void processArgs(String[] args) {
        String mode = "";
        for (String arg : args) {
            switch (mode) {
                case "-s":
                    in_s = arg;
                    mode = "";
                    break;
                case "-g":
                    in_g = arg;
                    mode = "";
                    break;
                case "-c":
                    out_c = arg;
                    mode = "";
                    break;
                case "-m":
                    out_m = arg;
                    mode = "";
                    break;
                case "-hf":
                    heuristic = arg;
                    mode = "";
                    break;
                default:
                    mode = arg;
            }
        }
    }
    
    public static void initializeHeuristics() {
        heuristics = new Hashtable();
        heuristics.put("misplaced", new NPuzzleMisplaced());
        heuristics.put("manhattan", new NPuzzleManhattan(NPUZZLE_SIZE));
        heuristics.put("composite", new NPuzzleComposite(NPUZZLE_SIZE));
    }
    
    public static NPuzzleState initializeNPuzzle() {
        NPuzzleState root;
        
        CS180ME1Input startGet = new CS180ME1Input(in_s);
        CS180ME1Input goalGet = new CS180ME1Input(in_g);
        
        try {
            String start = startGet.valueStringFromFile();
            String goal = goalGet.valueStringFromFile();
        
            root = new NPuzzleState (NPUZZLE_SIZE, start, goal, heuristics.get(heuristic.toLowerCase()));

            return root;
        }
        catch (Exception e) {
            return null;
        }
    }
}
