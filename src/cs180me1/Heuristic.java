package cs180me1;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public interface Heuristic {
    public int getDistance(String value, String goal);
}