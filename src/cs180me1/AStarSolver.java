package cs180me1;

import java.util.HashSet;
import java.util.PriorityQueue;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class AStarSolver {
    public AStarState startState;
    public AStarState goalState;
    long startTime;
    long endTime;
    
    public AStarSolver(AStarState startState) {
        this.startState = startState;
        goalState = null;
    }
    
    public void solve() {
        startTime = System.currentTimeMillis();
        if (startState.isSolvable()) {
            HashSet<String> visited = new HashSet();
            PriorityQueue<AStarState> priorityQueue = new PriorityQueue();
            priorityQueue.add(startState);

            while (goalState == null && !priorityQueue.isEmpty()) {
                AStarState closestChild = priorityQueue.poll();
                visited.add(closestChild.value);
                closestChild.generateChildren(visited);
                for (AStarState child : closestChild.children) {
                    if (child.h == 0) {
                        goalState = child;
                    }
                    priorityQueue.add(child);
                }
            }
            
            if (goalState == null) {
                goalState = startState.generateUnsolvableChild("UNSOLVABLE");
            }
        }
        else {
            goalState = startState.generateUnsolvableChild("UNSOLVABLE");
        }
        endTime = System.currentTimeMillis();
    }
    
    public long getTimeElapsed() {
        return endTime - startTime;
    }
            
}
