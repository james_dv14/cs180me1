package cs180me1;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class NPuzzleComposite implements Heuristic {
  public int size;

  public NPuzzleComposite(int size) {
    this.size = size;
  }

  @Override
  public int getDistance(String value, String goal) {
    NPuzzleMisplaced misplaced = new NPuzzleMisplaced();
    NPuzzleManhattan manhattan = new NPuzzleManhattan(size);
    return misplaced.getDistance(value, goal) + manhattan.getDistance(value, goal);
  }
}