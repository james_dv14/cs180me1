package cs180me1;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */

import java.util.HashSet;

public class AStarState implements Comparable {
    public String value;
    public String start;
    public String goal;
    public Heuristic heuristic;
    
    public int g;        // cost in steps from start state
    public int h;        // distance from goal state according to heuristic
    
    public HashSet<AStarState> children;
    public AStarState parent;
    
    // root constructor
    public AStarState (String start, String goal, Heuristic heuristic) {
        this.start = start;
        this.goal = goal;
        this.heuristic = heuristic;
        value = start;
        
        h = heuristic.getDistance(value, goal);
        g = 0;
    }
        
    // child constructor
    protected AStarState (AStarState parent, String value) {
        this.parent = parent;
        this.value = value;
        
        heuristic = parent.heuristic;
        start = parent.start;
        goal = parent.goal;
        
        if (!value.equals("UNSOLVABLE")) {
            h = heuristic.getDistance(value, goal);
        }
        else {
            h = Integer.MAX_VALUE - 1000;
        }
        g = parent.g + 1;
    }
    
    // abstract methods
    boolean isSolvable() { return true; }
    AStarState generateUnsolvableChild(String value) { return null; }
    void generateChildren(HashSet<String> visited) { }
    
    // Overrides
    @Override
    public int compareTo(Object o) {
        AStarState other = (AStarState) o;
        int total = g + h;
        int other_total = other.g + other.h;
        return Integer.compare(total, other_total);
    }
    
}
