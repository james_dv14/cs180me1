package cs180me1;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class NPuzzleMisplaced implements Heuristic {

    @Override
    public int getDistance(String value, String goal) {
        if (value.equals(goal)) {
            return 0;
        }
        int distance = 0;
        for (int i = 0; i < goal.length(); i++) {
            char letter = goal.charAt(i);
            int pos = value.indexOf(letter);
            if (pos != i) {
                distance += 1;
            }
        }
        return distance;
    }

}
