package cs180me1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class CS180ME1Input {
    File file;
    Scanner scanner;
            
    public CS180ME1Input (String filepath) {
        file = new File(filepath);
    }
    
    public String valueStringFromFile() {
        String value = "";
        try {
            scanner = new Scanner(file);
            while(scanner.hasNextLine()) {
                String line = scanner.nextLine();
                value += line;
            }
        }
        catch (FileNotFoundException e) {
            System.err.println("Invalid input file.");
        }
        catch (Exception e) {
            System.err.println("Unknown error occurred.");
            e.printStackTrace();
        }   
        return value.replaceAll("\\s+", "");
    }
    
    public void close() {
        if (scanner != null) {
            scanner.close();
        }
    }
}
